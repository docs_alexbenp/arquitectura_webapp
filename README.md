# Arquitectura Web App
**Prueba de Arquitectura: 
Diseñe la arquitectura para una aplicación que recibirá 40.000 peticiones por minuto, con una BD de My SQL, y tendrá una interfaz para dispositivos móviles Y un sitio web en java, el sistema deberá permitir inscripciones de personas por un formulario web y una app en Android.**


La siguiente arquitectura se presenta en un modelo ServerLess sobre la nube de AWS o Azure.
Las principales características sobre el modelo presentado es la autonomía en el crecimiento de los recursos cuando se incrementen los usuarios que se registran y hacen uso del portal o del APP. 
También al utilizar las funciones de las nubes permite tener distribuidor e implementar más adelante o de manera inmediata un modelo de alta disponibilidad, 
Facilidad en actualización de las funcionalidades que se prestan en el APP o portal. 
Cuando no se utilicen los servicios no se pagan por ellos.

![Diagrama ArquitecturaTalataa](ArquitecturaTalataa.png)

**Dibuje el esquema de arquitectura especificando número de servidores, (de BD, backend, Procedimientos almacenados, Apis, Frontend, etc) especifique cada ítem. **

En este modelo no estaría enfocado en cantidad de servidores implementados, ya estaría orientado a las funciones que se requieran desarrollar por cada servicio prestado por el portal/App
Ejemplo de los casos que se pueden presentar son 
 Function/login
 Function/registerUsers
 Function/deleteUser
 Function/cancelAccount
 


**Especifique y justifique si usara Apirestful o webservice, y si estos deben contener consultas o procesos lógicos, especificando el servidor donde se alojarían. **

Utilizaria el protocolo de comunicación de RESTful para la interacción entre las funciones y cada función según su objetivo tendría la lógica específica, Estas se alojarán de manera autónoma por la nube correspondiente con parámetros que permiten definir la memoria y atributos para su funcionamiento.

**Especifique si utilizara separación de los servicios por api, aplicaciones distribuidas, bróker de mensajería o similares). **

Si se realizaria una clasificación de los servicios según sus objetivos y sus atributos, esto con el objetivo de tener claridad el inventario de la plataforma, las aplicaciones con el modelo de Funciones estarían distribuidas y con el modelo asíncrono para el registro se implementa un servicio de colas para que no genere tiempos altos para dar respuesta al usuario. 

**Deben explicar todas las decisiones que tomen, Beneficios y desventajas, Y el xq recomiendan esas decisiones. **

Actualmente en las nuevas arquitecturas para soluciones se orienta a un modelo en los que la escalabilidad,alta disponibilidad, seguridad, actualizaciones sea un tema que no dependa de la compañía que lo implementa, todo eso se delega a la nube quién garantiza que en el momento que lo necesitemos se pueda hacer uso y solo se paga lo que se usa. 

Lo que se conoce en el tema de administración de infraestructura sería un concepto diferente.Ya no es necesario aprovisionar servidores o estar pendiente de aplicar los parches que se requieren antes de ejecutar las aplicaciones.

La escalabilidad se ve reflejada cuando se pasa de procesar 40 mil peticiones por minuto a 100 mil por minuto y de manera dinámica cada servicio que se ve afectado por el volumen en el tráfico puede soportar esa nueva operación sin generar costos innecesarios sobre otros servicios que no requieren ese incremento en capacidad.


**Que seguridad perimetral recomiendan**

Implementar modelos de segmentación de los servicios públicos y privados, aplicar las reglas en las VPC o en la NVA segun la nube con la política que por defecto todo está cerrado y a medida que se necesite habilitar algo se configura. 

Un factor importante sobre esta arquitectura es la facilidad de prender y apagar procesos de trazabilidad sobre cada evento que se ejecuta en la nube para temas de auditoría, donde permite tener un detalle importante a nivel operativo como un detalle para hacer un seguimiento a novedades de seguridad que se requiera investigar.




